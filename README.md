# Python Monad Example

## Monad

"*Design Pattern* in which pipeline implementation are *abstracted* by
*wrapping* a value in a type." - [A Byte of
Code](https://www.youtube.com/c/AByteofCode)

## Setup

Run `python main.py` (no external dependencies).

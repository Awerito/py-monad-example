class Maybe:
    def __init__(self, value):
        self.value = value

    def bind(self, function):
        if self.value is None:
            return self

        try:
            value = function(self.value)
            # ValueErrors here
        except Exception as e:
            print(e)  # ExceptionErrors here
            return Maybe(None)

        if value is None:
            return Maybe(0)
        return Maybe(value)

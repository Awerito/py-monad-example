import re
import html
import httpx

from monad import Maybe


def main():

    client = httpx.Client(follow_redirects=True)

    while True:
        try:
            url_input = input("Enter a URL: ")
        except KeyboardInterrupt:
            print("\nBye!")
            exit(0)

        title = (
            Maybe(url_input)
            .bind(str.strip)
            .bind(client.get)
            .bind(lambda response: response.text)
            .bind(lambda text: re.search(r"<title>(.+)</title>", text))
            .bind(lambda match: match.group(1))
            .bind(html.unescape)
            .bind(print)
        )

        if title.value:
            print("Error on fetching title")


if __name__ == "__main__":
    main()
